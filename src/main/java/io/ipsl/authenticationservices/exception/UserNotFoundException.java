package io.ipsl.authenticationservices.exception;

public class UserNotFoundException extends Exception{

    public UserNotFoundException(String exceptionMessage){
        super(exceptionMessage);
    }

}
